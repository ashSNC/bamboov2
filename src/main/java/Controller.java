

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class Controller
{
    private TurretHandler model;
    private ControllerWindow view;

    public Controller(TurretHandler model, ControllerWindow view)
    {
        this.model = model;
        this.view = view;

        // listeners for drop-down and button
        view.addCommandSubmitListener(new CommandSubmitListener());
        view.addComboBoxSelectionListener(new CommandSelectionListener());
    }

    public class CommandSubmitListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e) {
            model.setCurrentSteeringMode();
        }
    }

    public class CommandSelectionListener implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent command)
        {
            if(command.getStateChange() == ItemEvent.SELECTED)
                model.updateCurrentSelection(command.getItem().toString());
        }
    }
}
