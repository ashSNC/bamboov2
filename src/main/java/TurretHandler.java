import snc.turret.enums.GimbalSteeringMode;
import snc.turret.mx.TurretMX;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class TurretHandler
{
    private ArrayList<Enum> MXSensorCommands;
    private Enum currentSelection;
    private TurretMX turretMX;

    public TurretHandler() {
        turretMX = new TurretMX();

        MXSensorCommands = CommandPopulator.PopulateCommandsHashMap.FromTurretMXGimbalSteeringModeEnums();

        try {
            turretMX.start(new Properties());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setCurrentSteeringMode()
    {
        turretMX.setGimbalMode((GimbalSteeringMode)currentSelection);
    }

    public void updateCurrentSelection(String e)
    {
        currentSelection = GimbalSteeringMode.valueOf(e);

    }

    public ArrayList<Enum> getCommandList()
    {

        return MXSensorCommands;
    }

}