import javax.swing.*;



public class Main {

    public static void main(String[] args)
    {
        TurretHandler model = new TurretHandler();
        ControllerWindow view = new ControllerWindow(model);
        Controller controller = new Controller(model, view);
        JFrame frame = new JFrame("App");
        frame.setDefaultCloseOperation((JFrame.EXIT_ON_CLOSE));
        frame.setContentPane(view.MainPanel);
        frame.setSize(500,500);
        frame.setVisible(true);
    }

}
