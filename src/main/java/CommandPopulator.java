
import snc.turret.enums.GimbalSteeringMode;

import java.util.ArrayList;


public class CommandPopulator
{
    public static class PopulateCommandsHashMap
    {
        public static ArrayList<Enum> FromTurretMXGimbalSteeringModeEnums()
        {
            ArrayList<Enum> MXSensorCommands = new ArrayList<Enum>();

            for(GimbalSteeringMode mode: GimbalSteeringMode.values())
            {
                MXSensorCommands.add(mode);
            }

            return MXSensorCommands;
        }
    }

    private static byte[] hexStringToByteArray(String s)
    {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2)
        {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
}