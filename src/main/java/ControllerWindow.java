import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;

public class ControllerWindow {
    public JPanel MainPanel;
    private JComboBox CommandDropDown;
    private JButton CommandSubmitButton;

    private TurretHandler model;

    public ControllerWindow(TurretHandler model)
    {
        this.model = model;
        populateCommandsToComboBox();
    }

    private void populateCommandsToComboBox()
    {
        model.getCommandList().forEach(commandName -> {
            CommandDropDown.addItem(commandName.name());
        });
    }

    public void addCommandSubmitListener(ActionListener listener)
    {
        CommandSubmitButton.addActionListener(listener);
    }

    public void addComboBoxSelectionListener(ItemListener listener)
    {
        CommandDropDown.addItemListener(listener);
    }

}
