import org.junit.jupiter.api.Test;
import snc.turret.enums.GimbalSteeringMode;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class TurretHandlerTest {

    @org.junit.jupiter.api.Test
    void TestCommandListLength() {
        TurretHandler th = new TurretHandler();
        ArrayList<Enum> list = th.getCommandList();
        list.size();
        assertEquals(1, GimbalSteeringMode.values().length);
    }
}